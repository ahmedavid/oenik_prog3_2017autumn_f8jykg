var _game_item_8cs =
[
    [ "GameItem", "class_battle_city_1_1_game_item.html", "class_battle_city_1_1_game_item" ],
    [ "Brick", "class_battle_city_1_1_brick.html", "class_battle_city_1_1_brick" ],
    [ "Target", "class_battle_city_1_1_target.html", "class_battle_city_1_1_target" ],
    [ "Tank", "class_battle_city_1_1_tank.html", "class_battle_city_1_1_tank" ],
    [ "Bullet", "class_battle_city_1_1_bullet.html", "class_battle_city_1_1_bullet" ],
    [ "PowerUp", "class_battle_city_1_1_power_up.html", "class_battle_city_1_1_power_up" ],
    [ "PowerupType", "_game_item_8cs.html#a1d39b01a596ce11ae7458a48f5422799", [
      [ "HP", "_game_item_8cs.html#a1d39b01a596ce11ae7458a48f5422799ae6fc8ce107f2bdf0955f021a391514ce", null ],
      [ "CLASS", "_game_item_8cs.html#a1d39b01a596ce11ae7458a48f5422799ac18e8f1f430ea227dbd63d0d9a2bc5fb", null ]
    ] ],
    [ "TankType", "_game_item_8cs.html#aea9af57ba15138144daa02097c1e5493", [
      [ "Heavy", "_game_item_8cs.html#aea9af57ba15138144daa02097c1e5493af9ffe67a20da9cbff56b420fca4bd491", null ],
      [ "Light", "_game_item_8cs.html#aea9af57ba15138144daa02097c1e5493a9914a0ce04a7b7b6a8e39bec55064b82", null ]
    ] ]
];