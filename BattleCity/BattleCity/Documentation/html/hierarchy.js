var hierarchy =
[
    [ "Application", null, [
      [ "BattleCity.App", "class_battle_city_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "BattleCity.GameScreen", "class_battle_city_1_1_game_screen.html", null ]
    ] ],
    [ "BattleCity.GameItem", "class_battle_city_1_1_game_item.html", [
      [ "BattleCity.Brick", "class_battle_city_1_1_brick.html", [
        [ "BattleCity.Target", "class_battle_city_1_1_target.html", null ]
      ] ],
      [ "BattleCity.Bullet", "class_battle_city_1_1_bullet.html", null ],
      [ "BattleCity.PowerUp", "class_battle_city_1_1_power_up.html", null ],
      [ "BattleCity.Tank", "class_battle_city_1_1_tank.html", null ]
    ] ],
    [ "BattleCity.GameLogic", "class_battle_city_1_1_game_logic.html", null ],
    [ "BattleCity.GameModel", "class_battle_city_1_1_game_model.html", null ],
    [ "Window", null, [
      [ "BattleCity.MainWindow", "class_battle_city_1_1_main_window.html", null ]
    ] ]
];