var class_battle_city_1_1_tank =
[
    [ "Tank", "class_battle_city_1_1_tank.html#a4d41e79e4ea3a8f06ae3e7f27cd0fcc4", null ],
    [ "MoveHor", "class_battle_city_1_1_tank.html#a5e862f44343f6accc4a5e048347903da", null ],
    [ "MoveVer", "class_battle_city_1_1_tank.html#a0d472ccc748e36c6115f9d5816c2a31a", null ],
    [ "TakeDamage", "class_battle_city_1_1_tank.html#a63b2bcea829d66564362bd1be79efb03", null ],
    [ "TakeDamage", "class_battle_city_1_1_tank.html#adae8181c9eb9b22e1f94fcc7944851d9", null ],
    [ "Bullets", "class_battle_city_1_1_tank.html#ab5d6fa52efd0685fffbff39bd0b12ad8", null ],
    [ "CanShoot", "class_battle_city_1_1_tank.html#a6ceec3dfbf65587767f40af8862e7e1e", null ],
    [ "Color", "class_battle_city_1_1_tank.html#a3251aaf88bd35fd810e1195983df697a", null ],
    [ "direction", "class_battle_city_1_1_tank.html#acf4a7f10c1f9ee1def0d70992c6c955d", null ],
    [ "HP", "class_battle_city_1_1_tank.html#a2cdd815fa16d88643dd952c7f65cfbd5", null ],
    [ "IsAlive", "class_battle_city_1_1_tank.html#aa6ffca79773fead6f128a2ad680708ec", null ]
];