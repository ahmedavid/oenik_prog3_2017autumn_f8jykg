﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace BattleCity
{
    class GameModel
    {
        /// <summary>
        /// User for generation of Random integer for random location or chance to spawn a powerup
        /// or where to generate next spawning enemies.
        /// </summary>
        static Random R = new Random();
        /// <summary>
        /// Width and height of Game Screen
        /// </summary>
        public double W, H;
        /// <summary>
        /// Holds list of spawned enemy tanks
        /// </summary>
        public List<Tank> Enemies;
        /// <summary>
        /// Holds list of all tanks including player tank
        /// </summary>
        public List<Tank> AllTanks;
        /// <summary>
        /// Represents Players Tank
        /// </summary>
        public Tank Player;
        /// <summary>
        /// list of powerups that are spawned
        /// </summary>
        public List<PowerUp> PowerUps;
        /// <summary>
        /// List of all bricks. Both destructible and undestructible
        /// </summary>
        public List<Brick> Bricks;
        /// <summary>
        /// Represents Target that if destroyed game will end. It can take damage from player and other enemy tanks.
        /// Players job is to protect this.
        /// </summary>
        public Target Target;
        /// <summary>
        /// List of points represents where are new tanks are spawned.
        /// </summary>
        List<Point> SpawnLocations;


        /// <summary>
        /// Takes Width and Height of Game Screen
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public GameModel(double w, double h)
        {
            W = w; H = h;
            Init();
        }
        /// <summary>
        /// Method is used to Initialize fields and properties.
        /// This is set up so that game can be resetted when user presses "R"
        /// </summary>
        void Init()
        {
            Bricks = new List<Brick>();
            LoadLevel("2.lvl");

            SpawnLocations = new List<Point>()
            {
                new Point(10,10),
                new Point(W/2,10),
                new Point(W-40,10)
            };


            PowerUps = new List<PowerUp>();

            Player = new Tank(W / 4, H - 30) { Color = Brushes.Blue, HP = 3 };
            //Enemies = new List<Tank>()
            //{
            //    new Tank(W/2,102){ Color=Brushes.Red},
            //    new Tank(W/4,113){ Color=Brushes.Red},
            //    new Tank(W/2,204,TankType.Heavy){ Color=Brushes.Red},
            //    new Tank(W/2,102){ Color=Brushes.Red},
            //    new Tank(W/4,113){ Color=Brushes.Red},
            //    new Tank(W/2,204,TankType.Heavy){ Color=Brushes.Red},
            //};

            Enemies = new List<Tank>();
            SpawnEnemies(3);


            AllTanks = new List<Tank>();
            foreach (var enemy in Enemies)
            {
                AllTanks.Add(enemy);
            }

            AllTanks.Add(Player);
        }
        /// <summary>
        /// Method to spawn enemies in random location on top of the game screen.
        /// There is half and half chance to spawn Heavy or Light type tank.
        /// </summary>
        /// <param name="num"></param>
        public void SpawnEnemies(int num)
        {
            for (int i = 0; i < num; i++)
            {
                var rand = R.Next(0, 3);
                int chance = R.Next(0, 2);
                TankType t;
                if (chance == 0)
                {
                    t = TankType.Heavy;
                }
                else
                {
                    t = TankType.Light;
                }
                var enemy = new Tank(SpawnLocations[rand].X, SpawnLocations[rand].Y, t);
                enemy.Rad = Math.PI;
                enemy.Color = Brushes.Red;
                Enemies.Add(enemy);
                if (AllTanks != null)
                {
                    AllTanks.Add(enemy);
                }
            }
        }
        /// <summary>
        /// Spawns a power up item , which can be picked up by player.
        /// Spawn location is random.
        /// </summary>
        void SpawnPowerup()
        {
            int chance = R.Next(0, 3);
            if (chance == 1)
            {
                var randomX = R.Next(100, 500);
                var randomY = R.Next(100, 500);
                PowerUps.Add(new PowerUp(randomX, randomY));
            }
        }
        /// <summary>
        /// Called with a given tank , in return calls tanks damage method to decrease its hp.
        /// If tank is dead a powerup is spawned somewhere random on the screen.
        /// </summary>
        /// <param name="tank"></param>
        public void DamageEnemy(Tank tank)
        {
            tank.TakeDamage();
            if (!tank.IsAlive)
            {
                SpawnPowerup();
            }
        }
        /// <summary>
        /// When R Key is pressed on the keyboard , Game is resetted to starting state.
        /// </summary>
        public void Reset()
        {
            Init();
        }
        /// <summary>
        /// Reads a level text file and constructs Game play arena.
        /// T represents Target
        /// B represents normal brick
        /// S represents indestructible brick
        /// </summary>
        /// <param name="fname"></param>
        void LoadLevel(string fname)
        {

            string[] lines = File.ReadAllLines(fname);
            int xsize = int.Parse(lines[0]);
            int ysize = int.Parse(lines[1]);
            bool[,] walls = new bool[xsize, ysize];

            for (int x = 0; x < xsize; x++)
            {
                for (int y = 0; y < ysize; y++)
                {
                    if (lines[y + 2][x] == 'T')
                    {
                        Target = new Target(20 * x, 20 * y, true) { Color = Brushes.Chocolate };
                        Bricks.Add(Target);
                    }
                    if (lines[y + 2][x] == 'B')
                    {
                        Bricks.Add(new Brick(20 * x, 20 * y, true) { Color = Brushes.Brown });
                    }
                    if (lines[y + 2][x] == 'S')
                    {
                        Bricks.Add(new Brick(20 * x, 20 * y, false) { Color = Brushes.Gray });
                    }
                }
            }
        }
    }
}
