﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace BattleCity
{
    /// <summary>
    /// Base class for all Geometry Drawing Objects.Contains commong properties for every object like Color
    /// If is alive or not. Position on Arena,Width and Height of object.
    /// 
    /// Geometry Area represents geometry area for collison calculations and rendering.
    /// </summary>
    abstract class GameItem
    {
        public Brush Color;
        public bool IsAlive;
        public double CX;
        public double CY;
        public double W;
        public double H;
        double degree = 0;
        /// <summary>
        /// Rad gets and sets Radian values for degrees.
        /// </summary>
        public double Rad
        {
            get
            {
                return degree * Math.PI / 180;
            }
            set
            {
                degree = 180 * value / Math.PI;
            }
        }
        protected Geometry area;

        /// <summary>
        /// Applies Translation and rotation to current object.
        /// </summary>
        public Geometry RealArea
        {
            get
            {
                TransformGroup g = new TransformGroup();
                g.Children.Add(new TranslateTransform(CX, CY));
                g.Children.Add(new RotateTransform(degree, CX + W, CY + H));

                area.Transform = g;

                return area;
            }
        }
        /// <summary>
        /// Determines if current object has any intersection with given other object.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Boolean</returns>
        public bool IsCollision(GameItem other)
        {
            return Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }

    /// <summary>
    /// Represents Breakeable and unbreakable obstacles in game.
    /// Brown Bricks are destructible.Gray bricks are considered to be steel and are indestructable
    /// </summary>
    class Brick : GameItem
    {
        /// <summary>
        /// Determines if object is destructable
        /// </summary>
        public bool Destructible;
        /// <summary>
        /// Takes x position , y position and boolean if object should be desctructable or not.
        /// Initializes object are with a rectange geometry.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="destructible"></param>
        public Brick(double x, double y, bool destructible)
        {
            IsAlive = true;
            Destructible = destructible;
            CX = x;
            CY = y;
            area = new RectangleGeometry(new Rect(0, 0, Config.BRICK_WIDTH, Config.BRICK_HEIGHT));
        }
    }
    /// <summary>
    /// Derived from standard Brick class.Used to represent Target for enemy tanks.
    /// It can be destroyed by enemy tanks and also by friendly fire.
    /// </summary>
    class Target : Brick
    {
        /// <summary>
        /// Takes x position , y position and boolean if object should be desctructable or not.
        /// Reassigns area to a geometry more suitable to be a Target Object.
        /// It is a double size of standard brick.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="destructible"></param>
        public Target(double x, double y, bool destructible) : base(x, y, destructible)
        {
            area = new RectangleGeometry(new Rect(0, 0, Config.BRICK_WIDTH * 2, Config.BRICK_HEIGHT * 2));
        }
    }
    /// <summary>
    /// Identifies Tank Type. Heavy Type is slower but has more armor
    /// Light type has one armor but it can move faster
    /// </summary>
    public enum TankType { Heavy, Light }
    /// <summary>
    /// Represents all tanks in the game. Both Player tank and enemy tanks are generated from this.
    /// </summary>
    class Tank : GameItem
    {
        /// <summary>
        /// Tank type is Heavy or Light Class
        /// </summary>
        TankType type;
        /// <summary>
        /// Rendered Color , Player is Blue , Enemies ara Red
        /// </summary>
        public new Brush Color;
        /// <summary>
        /// When a tank fires a bullet it is added to list and managed.
        /// </summary>
        public List<Bullet> Bullets;
        /// <summary>
        /// Determines how fast a tank can move in the field.
        /// </summary>
        double speed;
        /// <summary>
        /// Determines which direction is tank facing {North,South,East,West}
        /// </summary>
        public int direction;
        /// <summary>
        /// Prevents spamming Space button to shoot stream of bullets.
        /// If a bullet havent made a contact cant fire another bullet.
        /// </summary>
        public bool CanShoot;
        /// <summary>
        /// How much hitpoints
        /// </summary>
        private int hp;
        /// <summary>
        /// Heavy tanks have 3 HP. Light tanks have 1 hp.
        /// </summary>
        public int HP
        {
            get { return hp; }
            set { hp = value; }
        }
        /// <summary>
        /// If tanks health is zero it means its destroyed and no longer rendered or managed by game logic.
        /// </summary>
        public new bool IsAlive
        {
            get
            {
                return hp > 0;
            }
        }

        /// <summary>
        /// Sets x and y position. Determines what type of tank will be initialized.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="type"></param>
        public Tank(double x, double y, TankType type = TankType.Light)
        {
            Bullets = new List<Bullet>();
            CX = x;
            CY = y;
            direction = 0;
            CanShoot = true;
            this.type = type;
            GeometryGroup g = new GeometryGroup();
            
            switch (this.type)
            {
                case TankType.Heavy:
                    speed = Config.LOW_SPEED;
                    W = Config.HEAVY_TANK_W / 2;
                    H = Config.HEAVY_TANK_W / 2;
                    g.Children.Add(new RectangleGeometry(new Rect(0, 0, Config.HEAVY_TANK_W, Config.HEAVY_TANK_W)));
                    g.Children.Add(new RectangleGeometry(new Rect(11.5, -20, 5, 20)));
                    g.Children.Add(new RectangleGeometry(new Rect(19.5, -20, 5, 20)));
                    area = g;
                    hp = 3;
                    break;
                case TankType.Light:
                    speed = Config.NORMAL_SPEED;
                    W = Config.LIGHT_TANK_W / 2;
                    H = Config.LIGHT_TANK_W / 2;
                    g.Children.Add(new RectangleGeometry(new Rect(0, 0, Config.LIGHT_TANK_W, Config.LIGHT_TANK_W)));
                    g.Children.Add(new RectangleGeometry(new Rect(12.5, -15, 5, 15)));
                    area = g;
                    hp = 1;
                    break;
            }
        }
        /// <summary>
        /// When a tank is in contact with a bullet its hp is reduced by i
        /// </summary>
        public void TakeDamage()
        {
            if (hp > 0) hp--;
        }
        /// <summary>
        /// Overloaded Method is used to damage the player and reset its position to the starting position.
        /// </summary>
        /// <param name="t"></param>
        public void TakeDamage(Tank t)
        {
            if (hp > 0) hp--;
            t.CX = Config.PLAYER_SPAWN_X;
            t.CY = Config.PLAYER_SPAWN_Y;
            Rad = 0;
        }
        /// <summary>
        /// Takes a double and calculates new Horizontal position.
        /// </summary>
        /// <param name="x"></param>
        public void MoveHor(double x)
        {
            CX += x * speed;
        }
        /// <summary>
        /// Takes a double and calculates new Vertical position.
        /// </summary>
        /// <param name="y"></param>
        public void MoveVer(double y)
        {
            CY += y * speed;
        }
    }
    /// <summary>
    /// Represents projectiles fired from tanks.Can impact other tanks and bricks and Target object as well.
    /// Upon impact does 1 damage.
    /// </summary>
    class Bullet : GameItem
    {
        /// <summary>
        /// Determines which direction is bullet fired
        /// </summary>
        public int direction;
        /// <summary>
        /// Starting Position X , Starting Position Y , bullet travel direction
        /// </summary>
        /// <param name="startX"></param>
        /// <param name="startY"></param>
        /// <param name="direction"></param>
        public Bullet(double startX, double startY, int direction)
        {
            IsAlive = true;
            this.direction = direction;
            CX = startX - 2;
            CY = startY - 2;
            area = new RectangleGeometry(new Rect(0, 0, 5, 5));
        }
    }
    /// <summary>
    /// Powerup Type can Have HP upgrade or Class upgrade {Light,Heavy}
    /// </summary>
    public enum PowerupType { HP, CLASS }
    /// <summary>
    /// Derived from GameItem. Represents power up objects , when picked up by player 
    /// adds +1 HP to Player health.
    /// </summary>
    class PowerUp : GameItem
    {
        /// <summary>
        /// Represents Powerup Icon (Star)
        /// </summary>
        public string Text;
        /// <summary>
        /// Power up type 
        /// </summary>
        PowerupType type;
        /// <summary>
        /// Determines x and y position and PowerupType
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="t"></param>
        public PowerUp(double x, double y, PowerupType t = PowerupType.HP)
        {
            IsAlive = true;
            type = t;
            switch (type)
            {
                case PowerupType.HP:
                    Text = (char)9733 + "";
                    break;
                case PowerupType.CLASS:
                    Text = "S";
                    break;
            }

            CX = x;
            CY = y;
            area = new RectangleGeometry(new Rect(0, 0, 30, 30));
        }
        /// <summary>
        /// When player picks up a powerup , determines what should happen.
        /// Currently player can pickup +1HP or upgrade its class to a Heavy class.
        /// </summary>
        /// <param name="tank"></param>
        public void Upgrade(Tank tank)
        {
            switch (type)
            {
                case PowerupType.HP:
                    tank.HP++;
                    break;
                case PowerupType.CLASS:
                    var x = tank.CX;
                    var y = tank.CY;
                    //tank = new Tank(x, y, TankType.Heavy);
                    break;
            }
            IsAlive = false;
        }
    }
}
