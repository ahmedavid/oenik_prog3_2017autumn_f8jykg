﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleCity
{
    /// <summary>
    /// Constans and game configuration
    /// </summary>
    public static class Config
    {
        public static double BRICK_WIDTH = 20;
        public static double BRICK_HEIGHT = 20;
        public static double BULLET_POS_SPEED = 40;
        public static double BULLET_NEG_SPEED = -40;
        public static double TANK_POS_SPEED = 4;
        public static double TANK_NEG_SPEED = -4;

        public static double HEAVY_TANK_W = 35;
        public static double LIGHT_TANK_W = 30;

        public static double NORMAL_SPEED = 1;
        public static double LOW_SPEED = 0.5;

        public static double PLAYER_SPAWN_X = 150;
        public static double PLAYER_SPAWN_Y = 570;
    }
}
