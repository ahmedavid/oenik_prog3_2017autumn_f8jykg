﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace BattleCity
{
    /// <summary>
    /// Represents Game Play Window
    /// </summary>
    class GameScreen : FrameworkElement
    {
        /// <summary>
        /// Instance of game model represention all objects
        /// </summary>
        GameModel model;
        /// <summary>
        /// Instance of gameplay logic responsible all movement and actions
        /// </summary>
        GameLogic logic;
        /// <summary>
        /// Listens for loaded event and intializes the screen
        /// </summary>
        public GameScreen()
        {
            Loaded += GameScreen_Loaded;
        }
        /// <summary>
        /// When window is loaded , initalizes all fields and properties.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameScreen_Loaded(object sender, RoutedEventArgs e)
        {
            model = new GameModel(ActualWidth, ActualHeight);
            logic = new GameLogic(model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                //Listen for Keyboard button pressed events
                win.KeyDown += Win_KeyDown;
                //Set up a timer for main game loop
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(40);
                timer.Tick += Timer_Tick;
                timer.Start();
            }
        }
        /// <summary>
        /// Use Up,Down,Left,Right keys to move around
        /// Press space to shoot 
        /// Press R to reset the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.R:
                    model.Reset();
                    break;
                case Key.Up:
                    logic.MoveUp(model.Player);
                    break;
                case Key.Down:
                    logic.MoveDown(model.Player);
                    break;
                case Key.Left:
                    logic.MoveLeft(model.Player);
                    break;
                case Key.Right:
                    logic.MoveRight(model.Player);
                    break;
                case Key.Space:
                    logic.Shoot(model.Player);
                    break;
            }
            InvalidateVisual();
        }
        /// <summary>
        /// Update all logic methods and clean the screen for new drawing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            logic.UpdateAll();
            InvalidateVisual();
        }
        /// <summary>
        /// This is where the magic happens and everything is drawen to the screen.
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (model != null)
            {
                if (model.Target.IsAlive && model.Player.IsAlive)
                {
                    if (model.Enemies.FindAll(x => x.IsAlive).Count > 0)
                    {
                        ///////////////DRAW INFO TEXT////////////////
                        string infoText = "";
                        infoText += "W:" + ActualWidth;
                        infoText += "\nH:" + ActualHeight;
                        infoText += "\nEnemy Count:" + model.Enemies.Count;
                        DrawText(drawingContext, infoText, 12, 10, 10);

                        //DRAW ALL BRICKS
                        foreach (var brick in model.Bricks)
                        {
                            if (brick.IsAlive)
                            {
                                drawingContext.DrawGeometry(brick.Color, null, brick.RealArea);
                            }
                        }
                        //DRAW ALL POWERUPS
                        foreach (var pwrp in model.PowerUps)
                        {
                            if (pwrp.IsAlive)
                            {
                                drawingContext.DrawGeometry(null, new Pen(Brushes.Gold, 3), pwrp.RealArea);
                                FormattedText pwrpText = new FormattedText(pwrp.Text, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 32, Brushes.Gold);
                                drawingContext.DrawText(pwrpText, new Point(pwrp.CX + 2, pwrp.CY - 5));
                            }
                        }
                        //DRAW ALL TANKS including player tank
                        foreach (var tank in model.AllTanks)
                        {
                            if (tank.IsAlive)
                            {
                                drawingContext.DrawGeometry(tank.Color, null, tank.RealArea);
                                DrawText(drawingContext, tank.HP + "", 12, tank.CX + tank.W - 5, tank.CY + tank.W - 5);

                                foreach (var bullet in tank.Bullets)
                                {
                                    if (bullet.IsAlive)
                                    {
                                        drawingContext.DrawGeometry(Brushes.Firebrick, null, bullet.RealArea);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ///////////////GAME WIN MENU////////////////
                        DrawText(drawingContext, "YOU WIN! \nPress R to Restart", 24, ActualWidth / 4, ActualHeight / 2);
                    }
                }
                else
                {
                    ///////////////GAME OVER MENU////////////////
                    DrawText(drawingContext, "GAME OVER! \nPress R to Restart", 24, ActualWidth / 4, ActualHeight / 2);
                }
            }
        }
        /// <summary>
        /// Method to dry up drawing text on screen
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="text"></param>
        /// <param name="fontSize"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void DrawText(DrawingContext drawingContext, string text, int fontSize, double x, double y)
        {
            FormattedText hud = new FormattedText(text, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), fontSize, Brushes.Black);
            drawingContext.DrawText(hud, new Point(x, y));
        }
    }
}
