﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace BattleCity
{
    /// <summary>
    /// Represents game play logic.
    /// Movement of the tanks and bullets.
    /// Checking for collisions etc.
    /// </summary>
    class GameLogic
    {
        /// <summary>
        /// Instantiate a GameModel object which represents every object in the game.
        /// </summary>
        public GameModel model;
        /// <summary>
        /// For each enemy tank a random direction is generated and stored int the array
        /// </summary>
        int[] randomDirection;
        /// <summary>
        /// Array of points which tanks was at previous frame
        /// </summary>
        Point[] lastPosArr;
        Random R;
        /// <summary>
        /// Enemy tanks periodicly change direction , move and shoot
        /// </summary>
        DispatcherTimer timer;
        /// <summary>
        /// Every few seconds a new enemy tank is spawned depending on the current numer of tanks on the screen,
        /// </summary>
        DispatcherTimer SpawnTimer;
        /// <summary>
        /// Constructor accepts a model object
        /// </summary>
        /// <param name="model"></param>
        public GameLogic(GameModel model)
        {
            this.model = model;
            lastPosArr = new Point[30];
            setLastPos();

            R = new Random();
            randomDirection = new int[30];
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();

            SpawnTimer = new DispatcherTimer();
            SpawnTimer.Interval = TimeSpan.FromSeconds(10);
            SpawnTimer.Tick += SpawnTimer_Tick;
            SpawnTimer.Start();
        }
        /// <summary>
        /// If number of current enemies is less than 5 and total enemies are less than 12 , Spawn an enemy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpawnTimer_Tick(object sender, EventArgs e)
        {
            if (model.Enemies.FindAll(x => x.IsAlive).Count < 5 && model.Enemies.Count < 12)
            {
                model.SpawnEnemies(1);
            }
        }
        /// <summary>
        /// Randomly choose a direction and take a shot.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < model.Enemies.Count; i++)
            {
                Tank enemy = model.Enemies[i];
                randomDirection[i] = R.Next(0, 6);
                Shoot(enemy);
            }
        }
        /// <summary>
        /// Sets last position of tanks so that when collision is detected object is teleported to last known location
        /// </summary>
        void setLastPos()
        {
            for (int i = 0; i < model.AllTanks.Count; i++)
            {
                lastPosArr[i].X = model.AllTanks[i].CX;
                lastPosArr[i].Y = model.AllTanks[i].CY;
            }

        }
        /// <summary>
        /// This method is called each time frame is rendered.
        /// In return it calls method to process all logic and Set last positions.
        /// </summary>
        public void UpdateAll()
        {

            UpdateTanks();
            setLastPos();

        }

        /// <summary>
        /// Checks if given object is out of boundaries of Game Arena
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool OutOfBoundaries(GameItem obj)
        {
            return obj.CX < 0 || obj.CX > model.W || obj.CY < 0 || obj.CY > model.H;
        }
        /// <summary>
        /// Given an object and a tank, this method teleports the tank to its last safe location
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="tank"></param>
        private void checkCollision(GameItem obj, Tank tank)
        {
            if (obj.IsCollision(tank))
            {
                var index = model.AllTanks.IndexOf(tank);
                if (
                    obj.RealArea.Bounds.Right > tank.RealArea.Bounds.Left &&
                    obj.RealArea.Bounds.Right < tank.RealArea.Bounds.Right)
                {

                    tank.CX = lastPosArr[index].X;
                    //tank.Color = Brushes.Green;
                }

                if (
                    obj.RealArea.Bounds.Right > tank.RealArea.Bounds.Left &&
                    obj.RealArea.Bounds.Right > tank.RealArea.Bounds.Right)
                {
                    tank.CX = lastPosArr[index].X;
                    //tank.Color = Brushes.RosyBrown;
                }

                if (
                    obj.RealArea.Bounds.Top < tank.RealArea.Bounds.Bottom &&
                    obj.RealArea.Bounds.Top > tank.RealArea.Bounds.Top)
                {
                    tank.CY = lastPosArr[index].Y;
                    //tank.Color = Brushes.Pink;
                }

                if (
                    obj.RealArea.Bounds.Top < tank.RealArea.Bounds.Bottom &&
                    obj.RealArea.Bounds.Top < tank.RealArea.Bounds.Top)
                {
                    tank.CY = lastPosArr[index].Y;
                    //tank.Color = Brushes.Honeydew;
                }
            }
        }
        /// <summary>
        /// Game loop for all objects in the game
        /// </summary>
        void UpdateTanks()
        {
            //loop over all tanks including enemies and player
            for (int i = 0; i < model.AllTanks.Count; i++)
            {
                Tank tank = model.AllTanks[i];
                if (tank.IsAlive)
                {
                    foreach (var brick in model.Bricks)
                    {
                        if (brick.IsAlive && brick.IsCollision(tank))
                        {
                            checkCollision(brick, tank);
                        }
                    }


                    //moves an enemy tank in random direction,there is more chance to go down towards target
                    if (tank != model.Player)
                    {
                        switch (randomDirection[i])
                        {
                            case 0: MoveDown(tank); break;
                            case 1: MoveUp(tank); break;
                            case 2: MoveLeft(tank); break;
                            case 3: MoveRight(tank); break;
                            case 4: MoveDown(tank); break;
                            case 5: MoveDown(tank); break;
                            case 6: MoveDown(tank); break;
                        }
                    }
                    else
                    {
                        //If Player collides with a powerup , powerup is killed and player collects the benefits.
                        foreach (var pwrp in model.PowerUps)
                        {
                            if (pwrp.IsAlive && pwrp.IsCollision(model.Player))
                            {
                                pwrp.Upgrade(model.Player);
                            }
                        }

                    }


                    //Loop over the tanks bullets and process
                    foreach (var bullet in tank.Bullets)
                    {
                        if (bullet.IsAlive)
                        {
                            foreach (var enemy in model.AllTanks)
                            {
                                //Check collision with all tanks
                                if (enemy != tank && enemy.IsAlive && bullet.IsCollision(enemy))
                                {
                                    bullet.IsAlive = false;
                                    var tank1 = model.Enemies.IndexOf(tank);
                                    var tank2 = model.Enemies.IndexOf(enemy);
                                    if (!(tank1 > -1 && tank2 > -1))
                                    {
                                        if (enemy == model.Player)
                                        {
                                            //Use overloaded method to damage player
                                            enemy.TakeDamage(model.Player);
                                        }
                                        else
                                        {
                                            //Damage enemy with regular method
                                            model.DamageEnemy(enemy);
                                        }

                                    }

                                }
                            }

                            //Check collison of bricks and bullets.If bullet hits breakable brick both are destroyed
                            //otherwise if brick is indestructable just the bullet dies.
                            foreach (var brick in model.Bricks)
                            {
                                if (brick.IsAlive && bullet.IsCollision(brick))
                                {
                                    if (brick.Destructible)
                                    {
                                        brick.IsAlive = false;
                                    }
                                    bullet.IsAlive = false;
                                }
                            }
                            //Tank is allowed to shoot only if previous bullet is outof arena and dead
                            if (bullet.IsAlive && !OutOfBoundaries(bullet))
                            {
                                tank.CanShoot = false;
                            }
                            else
                            {
                                tank.CanShoot = true;
                                bullet.IsAlive = false;
                            }

                            //Determine which direction bullet is fired
                            switch (bullet.direction)
                            {
                                case 0:
                                    bullet.CY += Config.BULLET_NEG_SPEED;
                                    break;
                                case 1:
                                    bullet.CY += Config.BULLET_POS_SPEED;
                                    break;
                                case 2:
                                    bullet.CX += Config.BULLET_NEG_SPEED;
                                    break;
                                case 3:
                                    bullet.CX += Config.BULLET_POS_SPEED;
                                    break;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Shoot bullet obviously
        /// </summary>
        /// <param name="tank"></param>
        public void Shoot(Tank tank)
        {
            if (tank.CanShoot)
            {
                tank.Bullets.Add(new Bullet(tank.CX + 15, tank.CY + 15, tank.direction));
            }
        }
        /// <summary>
        /// Pretty self describing isn't it.Why do I have to document everything. It is boriiiiing......
        /// </summary>
        /// <param name="tank"></param>
        public void MoveUp(Tank tank)
        {
            if (tank.RealArea.Bounds.Top > 0)
            {
                tank.direction = 0;
                tank.Rad = 0;
                tank.MoveVer(Config.TANK_NEG_SPEED);
            }
        }
        /// <summary>
        /// Move down,rotate the tank to proper position and set direction and move.
        /// </summary>
        /// <param name="tank"></param>
        public void MoveDown(Tank tank)
        {
            if (tank.RealArea.Bounds.Bottom < model.H)
            {
                tank.direction = 1;
                tank.Rad = Math.PI;
                tank.MoveVer(Config.TANK_POS_SPEED);
            }

        }
        /// <summary>
        /// Move left,rotate the tank to proper position and set direction and move.
        /// </summary>
        /// <param name="tank"></param>
        public void MoveLeft(Tank tank)
        {
            if (tank.RealArea.Bounds.Left > 0)
            {
                tank.direction = 2;
                tank.Rad = 1.5 * Math.PI;
                tank.MoveHor(Config.TANK_NEG_SPEED);
            }
        }
        /// <summary>
        /// Move Right,rotate the tank to proper position and set direction and move.
        /// </summary>
        /// <param name="tank"></param>
        public void MoveRight(Tank tank)
        {
            if (tank.RealArea.Bounds.Right < model.W)
            {
                tank.direction = 3;
                tank.Rad = -1.5 * Math.PI;
                tank.MoveHor(Config.TANK_POS_SPEED);
            }

        }
    }
}
